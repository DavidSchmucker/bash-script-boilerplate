#!/bin/bash


# ===== META =====

NAME="Bash Script Boilerplate"
VERSION="0.4.0"
AUTHOR="David Schmucker <david@schmucker-uhlemann.de>"


# ===== TEXT COLORS =====

# tput setaf for usage in heredocs
# Black 0   Red 1   Green 2   Yellow 3    Blue 4
# Magenta 5 Cyan 6  White 7   Grey 8      ... 

GREEN=""
YELLOW=""
RED=""
RESET=""

command tput -V &> /dev/null
if [ $? -eq 0 ]; then
  GREEN=$(tput setaf 2)
  YELLOW=$(tput setaf 3)
  RED=$(tput setaf 1)
  RESET=$(tput sgr0)
fi

# terminal color for usage in echo -e
# Black  0;30  Dark Gray    1;30  Blue        0;34     Light Blue    1;34
# Green  0;32  Light Green  1;32  Cyan        0;36     Light Cyan    1;36
# Red    0;31  Light Red    1;31  Purple      0;35     Light Purple  1;35
# Brown  0;33  Yellow       1;33  Light Gray  0;37     White         1;37

GREEN_COLOR='\033[0;32m';
YELLOW_COLOR='\033[0;33m';
RED_COLOR='\033[0;31m';
NO_COLOR='\033[0m';


# ===== PREDEFINITIONS =====

SCRIPTDIR=$(cd `dirname $0` && pwd)

die()
{
  if [ $# -eq 1 ]; then
    DIE_ECHO=$1
    EXIT_CODE=1
  elif [ $# -eq 2 ]; then
    DIE_ECHO=$2
    EXIT_CODE=$1
  else
    DIE_ECHO=$@
    EXIT_CODE=1
  fi

  echo -e >&2 ${RED_COLOR}"${DIE_ECHO}"${NO_COLOR}
  exit ${EXIT_CODE}
}


# ===== METATEXT ====

metatext()
{
cat <<EOT

${GREEN}${NAME} (Version: ${VERSION})${RESET}
EOT
}

metatext


# ===== HELPTEXT =====
helptext()
{
cat <<EOT

  Usage: <command> [options] [--] [args...]
    <command> -- [args...]

    -f, --flag            A Flag
    -h, --help            Helptext
    -o, --option <value>  A Option
    -v, --version         Show Program Version

    args...               Arguments passed to script. Use -- args when first argument
                          starts with - or script is read from stdin
EOT
}


# ===== ARGUMENTS PROPERTIES =====

FLAG=0
OPTION="DEFAULT"
OPERAND="DEFAULT"


# ===== ARGUMENTS HANDLING =====

argumentNeedValue()
{
  if [ -z "$1" ]; then
    die "\n${RED_COLOR}The argument $2 need a value!${NO_COLOR}\n"
  fi
}

OBLIGATE_ARGUMENTS=""
LAST_ARGUMENT=0
while true; do

  if [ -z "$1" ]; then
    break
  fi

  if [ $LAST_ARGUMENT -eq 1 ]; then
    die "\n${RED_COLOR}The argument $1 isn't allowed there, the operand argument (--) must be the last argument!${NO_COLOR}\n" 
  fi

  case $1 in

    -v | --version )
      echo -e "\n${YELLOW_COLOR}${NAME}\nVersion: ${VERSION}${NO_COLOR}\n"
      exit 0
    ;;

    -h | --help )
      helptext
      exit 0
    ;;

    -f | --flag )
      FLAG=1
      OBLIGATE_ARGUMENTS="${OBLIGATE_ARGUMENTS}f"
      shift
    ;;

    -o | --option )
      shift
      OBLIGATE_ARGUMENTS="${OBLIGATE_ARGUMENTS}o"
      argumentNeedValue "$1" "-o|--option"
      OPTION=$1
      shift
    ;;
    
    -- )
      shift
      if [ -z "$1" ]; then
        OPERAND=""
        while read LINE; do
          OPERAND="${OPERAND}${LINE}"
        done < "${1:-/dev/stdin}"
      else
        OPERAND=$1
        LAST_ARGUMENT=1
      fi
      shift
    ;;

    * )
      die "\n${RED_COLOR}Unknown argument: $1 !${NO_COLOR}\n"
    ;;

  esac
done

if [[ ! $OBLIGATE_ARGUMENTS =~ f ]] || [[ ! $OBLIGATE_ARGUMENTS =~ o ]]; then
  	die "\n${RED_COLOR}Missing obligate argument!${NO_COLOR}\n"
fi


# ===== LOGIC =====


# ===== POST =====